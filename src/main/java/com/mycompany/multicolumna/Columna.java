/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.multicolumna;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author david
 */
public class Columna {

    static public void main(String[] args) {

        try {
            String dest = "./columna-pdf";
            PdfDocument pdfDoc = new PdfDocument(new PdfWriter(dest));
            Document doc = new Document(pdfDoc);

            Paragraph p = new Paragraph("Texto de la columna 1 Texto de la columna 1 Texto de la columna 1 Texto de la columna 1 Texto de la columna 1 Texto de la columna 1 Texto de la columna 1 Texto de la columna 1 Texto de la columna 1 Texto de la columna 1 Texto de la columna 1 Texto de la columna 1 Texto de la columna 1 Texto de la columna 1 Texto de la columna 1 Texto de la columna 1 Texto de la columna 1 Texto de la columna 1 Texto de la columna 1 Texto de la columna 1 Texto de la columna 1 Texto de la columna 1 Texto de la columna 1 Texto de la columna 1 Texto de la columna 1 ");
            
            // Esto acomoda la columna
            float x = 100f;
            float y = 100f;
            float ancho = 200f;
            
            p.setFixedPosition(x, y, ancho);

            doc.add(p);

            doc.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(Columna.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
